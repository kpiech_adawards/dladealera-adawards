<?php

function build_mail_body($post){
  $content = file_get_contents('./templates/mail.html');
  foreach($post as $k => $v){
    $content = str_replace('{'.$k.'}', $v, $content);
  }

  return $content;
}